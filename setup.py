#!/usr/bin/env python
from distutils.extension import Extension
import distutils.core

import sys
import os
from os.path import join as pjoin


def clean(ext):
    pass

pwd = os.path.dirname(__file__)

includes = [os.path.join(pwd, 'hashmap')]
libs = [os.path.join(pwd, 'hashmap')]

exts = [
    Extension('hashmap', ["hashmap/hashmapmodule.cc"], language="c++",
              libraries = ['z'],
              include_dirs=includes),
]


if sys.argv[1] == 'clean':
    print >> sys.stderr, "cleaning .c, .c++ and .so files matching sources"
    map(clean, exts)

distutils.core.setup(
    description='python wrapper for sparsehash',
    name='hashmap',
    packages=['hashmap'],
    author='David Watson',
    author_email='david@wefreq.com',
    version='1.4',
    ext_modules=exts,
)
